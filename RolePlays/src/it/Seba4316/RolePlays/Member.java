package it.Seba4316.RolePlays;

import java.util.UUID;

public class Member {
	
	private UUID uuid;
	private Hierarchy hierarchy;
	
	public Member(UUID uuid, Hierarchy hierarchy) {
		this.uuid = uuid;
		this.hierarchy = hierarchy;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Hierarchy getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(Hierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}
	
}