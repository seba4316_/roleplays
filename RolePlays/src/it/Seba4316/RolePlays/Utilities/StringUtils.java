package it.Seba4316.RolePlays.Utilities;

public class StringUtils {

	public static String colorFormatter(String string) {
		return string.replace("&", "�");
	}

	public static String stringReplacer(String string, String[] parameters, String[] values) {
		String value = string;
		for (int i = 0; i < parameters.length; i++) {
			value.replace(parameters[i], values[i]);
		}
		return value;
	}

	public static String stringReplacerColorized(String string, String[] parameters, String[] values) {
		return colorFormatter(stringReplacer(string, parameters, values));
	}

	public static boolean isAlphaNumeric(String string) {

		return true;
	}

	public static boolean lengthInRange(String string, int min, int max) {
		boolean inRange = true;
		if (string.length() < min)
			inRange = false;
		if (string.length() > max)
			inRange = false;
		return inRange;
	}

}