package it.Seba4316.RolePlays.Utilities;

import org.bukkit.configuration.file.FileConfiguration;

import it.Seba4316.RolePlays.Main;

public class Config {

	private static Main plugin = Main.getPlugin(Main.class);
	private static FileConfiguration config = plugin.getConfig();

	public static String getString(String path) {
		return config.getString(path);
	}

	public static int getInt(String path) {
		return config.getInt(path);
	}

	public static double getDouble(String path) {
		return config.getDouble(path);
	}

	public static float getFloat(String path) {
		return (float) config.getDouble(path);
	}

}