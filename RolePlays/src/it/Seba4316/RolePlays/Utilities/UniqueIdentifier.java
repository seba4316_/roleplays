package it.Seba4316.RolePlays.Utilities;

import java.util.ArrayList;
import java.util.List;

public class UniqueIdentifier {
	
	private static List<Integer> ids = new ArrayList<Integer>();
	private static final int RANGE = 50000;

	private static int index = 0;

	static {
		for (int i = 1; i < RANGE; i++) {
			ids.add(i);
		}
	}

	private UniqueIdentifier() {

	}

	public static int getIdentifier() {
		if (index > ids.size() - 1)
			index = 0;
		return ids.get(index++);
	}
	
	public static void recycle(int id) {
		ids.add(id);
	}
	
}