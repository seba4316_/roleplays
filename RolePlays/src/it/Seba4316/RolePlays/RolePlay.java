package it.Seba4316.RolePlays;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

import it.Seba4316.RolePlays.Utilities.UniqueIdentifier;

public class RolePlay {

	private final int ID;
	private String name;
	private int participants;
	private int maxParticipants;
	private boolean Private;
	private List<Member> members = new ArrayList<Member>();

	public RolePlay(Player creator, String name, int maxParticipants, boolean Private) {
		this.ID = UniqueIdentifier.getIdentifier();
		this.name = name;
		this.maxParticipants = maxParticipants;
		this.participants = 0;
		this.Private = Private;
		this.members.add(new Member(creator.getUniqueId(), Hierarchy.Member));
	}

	public int getID() {
		return ID;
	}

	public boolean isPrivate() {
		return Private;
	}

	public void setPrivate(boolean private1) {
		Private = private1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isMemberInRoleplay(Member member) {
		return this.members.contains(member);
	}

	public UUID getOwner() {
		UUID uuid = UUID.randomUUID();
		for (int i = 0; i < this.members.size(); i++) {
			Member member = this.members.get(i);
			Hierarchy hierarchy = member.getHierarchy();
			if (!(hierarchy.equals(Hierarchy.Owner)))
				continue;
			uuid = member.getUuid();
		}
		return uuid;
	}

	public int getMaxParticipants() {
		return maxParticipants;
	}

	public void setMaxParticipants(int maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	public int getParticipants() {
		return participants;
	}

	public void setParticipants(int participants) {
		this.participants = participants;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public void addMember(Member member) {
		this.members.add(member);
	}

	public void removeMember(Member member) {
		this.members.remove(member);
	}

}