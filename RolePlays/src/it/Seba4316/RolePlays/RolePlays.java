package it.Seba4316.RolePlays;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class RolePlays {
	private static List<RolePlay> RolePlays = new ArrayList<RolePlay>();

	public static void addRP(RolePlay rp) {
		RolePlays.add(rp);
	}

	public static void removeRP(RolePlay rp) {
		RolePlays.remove(rp);
	}

	public static List<RolePlay> getRPs() {
		return RolePlays;
	}
	
	public static void playerJoined(Player player, RolePlay rp) {
		rp.addMember(new Member(player.getUniqueId(), Hierarchy.Member));
	}

	public static boolean playerInRoleplay(Player player) {
		boolean inRP = false;
		for (int i = 0; i < RolePlays.size(); i++) {
			RolePlay rp = RolePlays.get(i);
			if (rp.isMemberInRoleplay(new Member(player.getUniqueId(), Hierarchy.Owner)) || rp.isMemberInRoleplay(new Member(player.getUniqueId(), Hierarchy.Admin)) || rp.isMemberInRoleplay(new Member(player.getUniqueId(), Hierarchy.Member)))
				inRP = true;
		}
		return inRP;
	}

}