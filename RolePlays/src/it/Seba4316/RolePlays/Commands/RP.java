package it.Seba4316.RolePlays.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import it.Seba4316.RolePlays.RolePlay;
import it.Seba4316.RolePlays.RolePlays;
import it.Seba4316.RolePlays.Utilities.Config;
import it.Seba4316.RolePlays.Utilities.ErrorUtils;
import it.Seba4316.RolePlays.Utilities.StringUtils;

public class RP implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String msg, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("");
			return true;
		}
		Player player = (Player) sender;
		if (args.length == 1) {
			player.sendMessage("");
			return true;
		}

		if (args.length >= 2) {

			if (args.length == 2) {
				switch (args[1]) {
				case "create":
					player.sendMessage(StringUtils.colorFormatter(Config.getString("Constructor.create")));
					break;
				default:
					break;
				}
			}

			if (args.length == 3) {
				switch (args[1]) {
				case "create":
					if (!(StringUtils.lengthInRange(args[2], 4, 16))) {
						player.sendMessage(
								StringUtils.colorFormatter(Config.getString("Commands.create.name.out_of_range")));
						break;
					}
					if (!(StringUtils.isAlphaNumeric(args[2]))) {
						player.sendMessage(
								StringUtils.colorFormatter(Config.getString("Commands.create.name.not_alphanumeric")));
						break;
					}
					try {
						RolePlays.addRP(new RolePlay(player, args[2], 16, true));
						player.sendMessage(StringUtils.stringReplacerColorized(
								Config.getString("Commands.create.created"), new String[] { "<creator>", "<name>" },
								new String[] { player.getName(), args[2] }));
					} catch (Exception ex) {
						player.sendMessage(ErrorUtils.error());
						ex.printStackTrace();
					}
					break;
				case "join":
					if (RolePlays.playerInRoleplay(player)) {
						player.sendMessage(StringUtils.colorFormatter(Config.getString("Commands.join.already_in_rp")));
						break;
					}
					try {
						RolePlay rp = null;
						for (int i = 0; i < RolePlays.getRPs().size(); i++) {
							RolePlay roleplay = RolePlays.getRPs().get(i);
							if(Bukkit.getPlayer(roleplay.getOwner()).getName().equalsIgnoreCase(args[2]))
								rp = roleplay;
							if(roleplay.getName().equalsIgnoreCase(args[2]))
								rp = roleplay;
						}
						RolePlays.playerJoined(player, rp);
						
					} catch (Exception ex) {
						player.sendMessage(StringUtils.colorFormatter(Config.getString("Commands.join.already_in_rp")));
						break;
					}
					break;
				default:
					break;
				}
			}

			if (args.length == 4) {

			}

		}
		player.sendMessage(Config.getString(""));
		return true;
	}

}